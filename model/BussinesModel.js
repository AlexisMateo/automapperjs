class BussinesModel{
    constructor(){
          this.createdAt = {},
          this.businessId= '',
          this.businessUrl= '',
          this.advertId= 0,
          this.subscriberId= 0,
          this.name= '',
          this.commercialName= '',
          this.order= 0,
          this.hasCoupon= false,
          this.hasReservation= false,
          this.acceptComment= 0,
          this.hasDiamond= 0,
          this.hasDeals= false,
          this.cityCode= '',
          this.categories= [
            {
              businessId: '',
              inversionWeight: 0,
              businessCount: 0,
              classCode: 0,
              name: '',
              sanitizedClassName: '',
              subClassCode: '',
              subName: ''
            }
          ],
          this.address= [
            {
              businessId: '',
              city: '',
              cityCode: '',
              distance: 0,
              id: 0,
              isPrincipal: 0,
              latitude: 0,
              longitude: 0,
              name: '',
              phoneId: 0
            }
          ],
          this.phones= [
            {
              addressId: 0,
              businessId: '',
              isPrincipal: 0,
              number: 0,
              phoneId: 0
            }
          ],
          this.emails= [
            {
              id: 0,
              businessId: '',
              name: ''
            }
          ],
          this.webs= [
            {
              id: 0,
              name: '',
              businessId: ''
            }
          ],
          this.items=[{
            businessId: "",
            classCode: "",
            description: "",
            id: 0,
            isFilter: 0,
            itemNameDescription: "",
            name: "",
            type: ""
          }]
        this.reviewsCount= 0,
        this.reviewsAverage= 0,
        this.isFavorite= 0,
        this.aboutBusiness= {
          description:{},
          paymentMethods: [
            {
              id: 0,
              name:  "p",
              image:  "p"
            }
          ],
          schedules: [
            {
              day: 0,
              dayName:"a",
              start: "a",
              finish: "a"
            }
          ],
          productAndServices: [{
            id: 0,
            name: "",
            type: ""
          }],
          specialities: [{
            id: 0,
            name: "",
            type: ""
          }]
        
        
        }
    }
          
    
}

module.exports = BussinesModel;
