class RestaurantModel{
    constructor(){
        this.name= "",
        this.commercialName= "",
        this.category = {
          businessId: "",
          inversionWeight: 0,
          businessCount: 0,
          classCode: 0,
          name: "",
          sanitizedClassName: "",
          subClassCode: "",
          subName: ""
        },
        this.address= {
          businessId: "",
          city: "",
          cityCode: "",
          distance: 0,
          id:0,
          isPrincipal: 0,
          latitude: 0,
          longitude: 0,
          name: "",
          phoneId: 0
        },
        this.phone= {
          addressId: 0,
          businessId: "",
          isPrincipal: 0,
          number: 0,
          phoneId: 0
        },
        this.imageUrl="" 
    }
}

module.exports = RestaurantModel;