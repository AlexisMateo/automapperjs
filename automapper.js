function isArray(value) {
  return value.constructor === Array;
}

function hasParentObject(object) {
  return !(object.__proto__ == null);
}


function automapper() {

  this.map = function(data){
    this.data = data;
    return this;
  }
  this.to = function(model){
    let newArray = [];
    let data = this.data;
    if (!isArray(data)) {
  
      const newObject = completeAssign(new model(), data);
      let mappedObject = Object.assign(new model(), newObject);
  
      return mappedObject;
  
    }
  
    data.map(object => {
      const newObject = completeAssign(new model(), object);
      let mappedObject = Object.assign(new model(), newObject);
      newArray.push(mappedObject);
    });
  
    return newArray;
  }



}


function completeAssign(target, ...sources) {
  let mappedObject = {};
  sources.forEach(source => {
    let descriptors = Object.keys(target).reduce((descriptors, key) => {
      let value = source[key];

      if (typeof value == "object" && value != null && value.hasOwnProperty()) {
        let innerObject = completeAssign(target[key], key, value);
        descriptors[key] = innerObject;
      }

      if (value) {
        if (Object.values(value).length != 0 || hasParentObject(value)) {
          descriptors[key] = source[key];
        } else {
          if (isArray(target)) {
            descriptors[key] = target[0][key];
          } else {
            descriptors[key] = target[key];
          }
        }

      } else {
        descriptors[key] = target[key];
      }
      return descriptors;
    }, {});

    mappedObject = descriptors;
  });
  return mappedObject;
}

module.exports = new automapper();