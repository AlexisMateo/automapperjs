const mockData = require('../mock/mockData');
const map = require('../automapper');
const RestaurantModel = require('../model/RestaurantModel');


test('debe retornar un array mapeado', ()=>{
    let data = mockData;
    let restaurants = map(RestaurantModel,data)
    expect(restaurants).not.toBeNull();
});

test('debe retornar un objecto mapeado', ()=>{
    let data = mockData;
    let restaurantOne = data[0]
    let restaurant = map(RestaurantModel,restaurantOne)
    expect(restaurant).not.toBeNull();
});