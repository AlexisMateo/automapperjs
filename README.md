# AUTOMAPPER-JS



AutoMapper-js is released under the [MIT license]() & supports modern environments.<br>

## Installation


Using npm:
```shell
$ npm i --save automapper-js
```

In Node.js:
```js
var mapper = require('automapper-js');
```

## Why AutoMapper-js?

AutoMapper makes JavaScript easier by taking the hassle out of working with object mapping manually.</br>

## Usage
first define your model. e.g
```js
    class Person{
        constructor(){
            this.FirstName = "",
            this.LastName = "",
            this.Age = "",
            this.Address = ""
        }
    }

    module.exports = Person;
```
</br>
Now lets map a JSON response to our Person Model. <br>
let's import our model (Person), automapper-js and axios.

```js
    const mapper = require('automapper-js');
    const PersonModel = require('./Models/Person');
    const axios = require('axios');

    async run()=>{
        let persons = await axios.get("https://some-api/persons");
        let newMappedObject = mapper(PersonModel, persons);
    }

    run();

```

</br>

made with love
