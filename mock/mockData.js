module.exports=[
    {
      createdAt: new Date(),
      unWanted: 'un Wanted property',
      businessId: '270640_2_JA',
      businessUrl: null,
      advertId: '1977123',
      subscriberId: 270640,
      name: 'Pizza y Pepperoni (Pizza & Pepperoni)',
      commercialName: 'pizza-y-pepperoni-pizza-pepperoni',
      order: 4000,
      hasCoupon: false,
      hasReservation: true,
      acceptComment: 1,
      hasDiamond: 0,
      hasDeals: false,
      cityCode: 'JA',
      categories: [
        {
          businessId: '270640_2_JA',
          inversionWeight: 4000,
          businessCount: 0,
          classCode: '111700',
          name: 'Pizzerías',
          sanitizedClassName: 'pizzerias',
          subClassCode: null,
          subName: null
        }
      ],
      address: [
        {
          businessId: '270640_2_JA',
          city: 'Jarabacoa',
          cityCode: 'JA',
          distance: 0,
          id: 1,
          isPrincipal: 1,
          latitude: 19.118023,
          longitude: -70.639337,
          name: 'G F Deligné 3, Próximo al colegio de las monjas',
          phoneId: 0
        }
      ],
      phones: [
        {
          addressId: 1,
          businessId: '270640_2_JA',
          isPrincipal: 1,
          number: '8095744348',
          phoneId: 2
        }
      ],
      emails: [
        {
          id: 0,
          businessId: '270640_2_JA',
          name: 'joseruben.piña@hotmail.com'
        }
      ],
      webs: [
        {
          id: 0,
          name: 'www.facebook.com/pizzaypeperoni',
          businessId: '270640_2_JA'
        }
      ],
      items: [
        {
          businessId: '270640_2_JA',
          classCode: '111700',
          description: ' Pizzería, repostería y restaurante. ',
          id: 0,
          isFilter: 0,
          itemNameDescription: 'Texto Adicional (Debajo del nombre)',
          name: ' Pizzería, repostería y restaurante. ',
          type: 'ADDITIONAL_TEXT'
        },
        {
          businessId: '270640_2_JA',
          classCode: '111700',
          description: 'www.facebook.com/pizzaypeperoni',
          id: 0,
          isFilter: 0,
          itemNameDescription: 'FACEBOOK',
          name: 'www.facebook.com/pizzaypeperoni',
          type: 'FACEBOOK'
        },
        {
          businessId: '270640_2_JA',
          classCode: '111700',
          description: 'www.instagram.com/pizzaypepperoni',
          id: 0,
          isFilter: 0,
          itemNameDescription: 'INSTAGRAM',
          name: 'www.instagram.com/pizzaypepperoni',
          type: 'INSTAGRAM'
        },
        {
          businessId: '270640_2_JA',
          classCode: '111700',
          description: '4066312',
          id: 0,
          isFilter: 0,
          itemNameDescription: 'LOGO',
          name: '4066312',
          type: 'LOGO'
        },
        {
          businessId: '270640_2_JA',
          classCode: '111700',
          description: '4066312',
          id: 0,
          isFilter: 0,
          itemNameDescription: null,
          name: '4066312',
          type: 'MAPA'
        },
        {
          businessId: '270640_2_JA',
          classCode: null,
          description: '',
          id: 0,
          isFilter: 0,
          itemNameDescription: 'Años de Membresía',
          name: '14',
          type: 'MEMBER_SINCE'
        },
        {
          businessId: '270640_2_JA',
          classCode: null,
          description: '270640_2_JA',
          id: 0,
          isFilter: 0,
          itemNameDescription: 'Menú',
          name: '270640_2_JA',
          type: 'MENU'
        },
        {
          businessId: '270640_2_JA',
          classCode: '111700',
          description: '@pizzaypepperoni',
          id: 0,
          isFilter: 0,
          itemNameDescription: 'Twitter',
          name: '@pizzaypepperoni',
          type: 'TWITTER'
        }
      ],
      reviewsCount: 0,
      reviewsAverage: 0,
      isFavorite: 0,
      aboutBusiness: null
    },
    {
      createdAt: new Date(),
      unWanted: 'un Wanted property',
      businessId: '10282598_1_SD',
      businessUrl: null,
      advertId: '10282598_1',
      subscriberId: 10282598,
      name: 'Pizzas',
      commercialName: 'pizzas',
      order: 0,
      hasCoupon: false,
      hasReservation: false,
      acceptComment: 1,
      hasDiamond: 0,
      hasDeals: false,
      cityCode: 'SD',
      categories: [
        {
          businessId: '10282598_1_SD',
          inversionWeight: 0,
          businessCount: 0,
          classCode: '111700',
          name: 'Pizzerías',
          sanitizedClassName: 'pizzerias',
          subClassCode: null,
          subName: null
        }
      ],
      address: [
        {
          businessId: '10282598_1_SD',
          city: 'Santo Domingo',
          cityCode: 'SD',
          distance: 0,
          id: 1,
          isPrincipal: 1,
          latitude: 0,
          longitude: 0,
          name: 'A Lugo 178',
          phoneId: 0
        }
      ],
      phones: [
        {
          addressId: 1,
          businessId: '10282598_1_SD',
          isPrincipal: 1,
          number: '8094723333',
          phoneId: 1
        }
      ],
      emails: [
        {
          id: 0,
          businessId: '10282598_1_SD',
          name: 'info@grupoloudas.com'
        }
      ],
      webs: [],
      items: [],
      reviewsCount: 0,
      reviewsAverage: 0,
      isFavorite: 0,
      aboutBusiness: null
    },
    {
      createdAt: new Date(),
      unWanted: 'un Wanted property',
      businessId: '10049786_1_LT',
      businessUrl: null,
      advertId: '10049786_1',
      subscriberId: 10049786,
      name: 'Pizza Playa',
      commercialName: 'pizza-playa',
      order: 0,
      hasCoupon: false,
      hasReservation: false,
      acceptComment: 1,
      hasDiamond: 0,
      hasDeals: false,
      cityCode: 'LT',
      categories: [
        {
          businessId: '10049786_1_LT',
          inversionWeight: 0,
          businessCount: 0,
          classCode: '111700',
          name: 'Pizzerías',
          sanitizedClassName: 'pizzerias',
          subClassCode: null,
          subName: null
        }
      ],
      address: [
        {
          businessId: '10049786_1_LT',
          city: 'Las Terrenas',
          cityCode: 'LT',
          distance: 0,
          id: 1,
          isPrincipal: 1,
          latitude: 19.32263,
          longitude: -69.54159,
          name: 'F A C Deño',
          phoneId: 0
        }
      ],
      phones: [
        {
          addressId: 1,
          businessId: '10049786_1_LT',
          isPrincipal: 1,
          number: '8092405841',
          phoneId: 1
        }
      ],
      emails: [],
      webs: [],
      items: [],
      reviewsCount: 0,
      reviewsAverage: 0,
      isFavorite: 0,
      aboutBusiness: null
    },
    {
      createdAt: new Date(),
      unWanted: 'un Wanted property',
      businessId: '250888_1_SD',
      businessUrl: null,
      advertId: '250888_1_1',
      subscriberId: 250888,
      name: 'Pizza Karen',
      commercialName: 'pizza-karen',
      order: 0,
      hasCoupon: false,
      hasReservation: false,
      acceptComment: 1,
      hasDiamond: 0,
      hasDeals: false,
      cityCode: 'SD',
      categories: [
        {
          businessId: '250888_1_SD',
          inversionWeight: 0,
          businessCount: 0,
          classCode: '111700',
          name: 'Pizzerías',
          sanitizedClassName: 'pizzerias',
          subClassCode: null,
          subName: null
        }
      ],
      address: [
        {
          businessId: '250888_1_SD',
          city: 'Santo Domingo',
          cityCode: 'SD',
          distance: 0,
          id: 1,
          isPrincipal: 1,
          latitude: 18.492455,
          longitude: -69.886182,
          name: 'E Manzueta 36',
          phoneId: 0
        }
      ],
      phones: [
        {
          addressId: 1,
          businessId: '250888_1_SD',
          isPrincipal: 1,
          number: '8095384878',
          phoneId: 1
        }
      ],
      emails: [],
      webs: [],
      items: [
        {
          businessId: '250888_1_SD',
          classCode: null,
          description: 'PROFILE',
          id: 0,
          isFilter: 0,
          itemNameDescription: 'Perfil',
          name: 'PROFILE',
          type: 'PROFILE'
        }
      ],
      reviewsCount: 0,
      reviewsAverage: 0,
      isFavorite: 0,
      aboutBusiness: null
    }
  ]