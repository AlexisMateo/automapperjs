const axios = require("axios");

async function fetchRestaurants(){
    let response = await axios.get('http://caribemediapaelapi.azurewebsites.net/api/elasticsearch/getPopularRestaurants/PAEL?limit=5');
    let {data:{data}} = response;
    return data;
}

async function searchBussines(){
    let response = await axios.post('http://caribemediapaelapi.azurewebsites.net/api/elasticsearch/search/PAEL',{
            page: 1,
            pageSize: 10,
            searchTerm: "pizza",
    });
    let {data:{data:{business}}} = response;
    let Odata =  {noDeaseble:"no deaseable",...business};

    return Odata;
}

module.exports = {
    fetchRestaurants,
    searchBussines
}
  